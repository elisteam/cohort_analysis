import time
import argparse
from datetime import timedelta, datetime
from multiprocessing import (
    cpu_count,
    Pool
)
from collections import OrderedDict

from analize_lib import convert_to_timestamp, convert_to_datetime

TIME_LAYOUT = "%Y-%m-%d %H:%M:%S"


def read_csv(path, delimeter=','):
    res = []
    with open(path, 'r') as f:
        ln = f.readline()
        while ln:
            ln = f.readline()
            res.append(ln.strip().split(delimeter))

    if not res[-1] or not res[-1][0]:
        del res[-1]

    return res


def write_csv(path, res, delimeter=',', head=None):
    with open(path, 'w') as f:
        if head:
            f.write(delimeter.join(iter(head)) + '\n')

        for ln in res:
            f.write(delimeter.join([str(i) for i in ln]) + '\n')


def parse_args():
    parser = argparse.ArgumentParser(description='Performs cohort analysis of installs and purchases')
    parser.add_argument(
        'app_id', type=int, 
        help='app_id for measurements')

    parser.add_argument(
        'results_file', nargs='?', type=str,
        default='results.csv', 
        help='output file with results, default: results.csv')

    parser.add_argument(
        'installs_file', nargs='?', type=str,
        default='installs.csv', 
        help='input file with installs, default: installs.csv')

    parser.add_argument(
        'purchases_file', nargs='?', type=str,
        default='purchases.csv', 
        help='input file with purchases, default: purchases.csv')

    parser.add_argument(
        'start_time', nargs='?', type=str,
        default='2016-05-02 00:00:00', 
        help='start time for measurements, default: 2016-05-02 00:00:00')

    parser.add_argument(
        'end_time', nargs='?', type=str,
        default='2016-05-09 23:59:59', 
        help='end time for measurements, default: 2016-05-09 23:59:59')

    parser.add_argument(
        'days', nargs='?', type=int,
        default=10, 
        help='number of days for PRI calculation, default: 10')

    args = parser.parse_args()
    return args


def main(inst_path, purch_path, result_path, start_date, end_date, app_id, days=10):
    _st = time.monotonic()

    app_id = int(app_id)

    df = read_csv(inst_path)

    print(len(df))

    exec_st = time.monotonic()
    p = Pool(cpu_count()*4)
    tst = p.map(convert_to_timestamp, [l[0] for l in df])

    # copy timestamps to df
    for i in range(len(df)):
        df[i][0] = tst[i]

        # convert mobile app id to int
        df[i][1] = int(df[i][1])

    # tst = tp.map(lambda ln: int(datetime.strptime(ln[0], '%Y-%m-%d %H:%M:%S').timestamp()), df)
    print('Convert time: {!s}'.format(timedelta(0, time.monotonic() - exec_st)))

    # for i in range(len(df)):
    #     df[i][0] = int(datetime.strptime(df[i][0], '%Y-%m-%d %H:%M:%S').timestamp())

    # for ln in df:
    #    ln[0] = datetime.strptime(ln[0], '%Y-%m-%d %H:%M:%S').timestamp()

    start = convert_to_timestamp(start_date)
    end = convert_to_timestamp(end_date)

    print(start, end)

    # select cohort
    df2 = []
    # df2 = [l for l in df if l[0] >= start and l[0] <= end and l[1] == app_id]
    for l in df:
        if end >= l[0] >= start and l[1] == app_id:
            df2.append(l)

    print(len(df2))

    # group by country
    countries = dict()
    for l in df2:
        try:
            countries[l[2]] += 1
        except KeyError:
            countries[l[2]] = 1

    countries = OrderedDict(sorted(
        countries.items(), key=lambda x: x[1], reverse=True))

    for k,v in countries.items():
        print(k, v)

    pur = read_csv(purch_path)
    print(len(pur))

    exec_st = time.monotonic()
    tst = [None, None]
    tst[0] = p.map(convert_to_timestamp, [p[0] for p in pur])
    tst[1] = p.map(convert_to_timestamp, [p[3] for p in pur])
    p.close()

    # copy timestamps to df
    for i in range(len(pur)):
        pur[i][0] = tst[0][i]
        pur[i][3] = tst[1][i]

        # convert mobile app id to int
        pur[i][1] = int(pur[i][1])

    # tst = tp.map(lambda ln: int(datetime.strptime(ln[0], '%Y-%m-%d %H:%M:%S').timestamp()), df)
    print('Convert time: {!s}'.format(timedelta(0, time.monotonic() - exec_st)))

    # select cohort in purchases
    pur2 = []
    for p in pur:
        if end >= p[3] >= start and p[1] == app_id:
            pur2.append(p)

    print(len(pur2))

    res = [[k, v] for k, v in countries.items()]
    for i in range(1, days+1):
        # ends.append(start + timedelta(1+i, -1))
        end = start + int(timedelta(i, -1).total_seconds())
        for country_row in res:
            revenue = 0.0
            for p in pur2:
                if p[3] <= end and p[2] == country_row[0]:
                    revenue += float(p[4])

            # print(country_row[:2], revenue)
            country_row.append(round(revenue/country_row[1], 1))

    for res_row in res:
        print('\t'.join([str(i) for i in res_row]   ))

    # import ipdb; ipdb.set_trace()
    # write res to csv
    write_csv(result_path, res,
        head=['country', 'installs'] + ['PRI{}'.format(i) for i in range(1, days+1)])


if __name__ == '__main__':
    args = parse_args()

    exec_st = time.monotonic()
    main(
        args.installs_file, args.purchases_file, args.results_file,
        args.start_time, 
        args.end_time,
        args.app_id,
        args.days,
    )
    print('Exec time: {!s}'.format(timedelta(0, time.monotonic() - exec_st)))


from datetime import timedelta, datetime

TIME_LAYOUT = "%Y-%m-%d %H:%M:%S"

def convert_to_timestamp(dt):
    return int(datetime.strptime(dt, TIME_LAYOUT).timestamp())


def convert_to_datetime(dt):
    return datetime.strptime(dt, TIME_LAYOUT)



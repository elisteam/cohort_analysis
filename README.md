# cohort analysis

## Python native implementation

### Run example

#### Minimal running command:

```bash
$ python3 analize.py 2 

```

After calculations result will be writed in _results.csv_

This behavior may be changed by CLI args, see **Help with argparse below.**

#### Full run example (whole output):

```bash
$ python3 analize.py 2
2000000
Convert time: 0:01:34.121854
1462136400 1462827599
593034
US 113547
UK 112569
AU 84765
NZ 84472
CA 84391
BY 28353
TH 28332
KZ 28309
RU 28296
1539793
Convert time: 0:02:16.571070
457244
US  113547  0.0 1.7 3.4 5.2 6.8 8.5 10.3    12.0    13.6    13.6
UK  112569  0.0 1.3 2.7 4.1 5.5 6.8 8.2 9.5 10.9    10.9
AU  84765   0.0 1.0 2.0 3.0 3.9 4.9 5.9 6.8 7.7 7.7
NZ  84472   0.0 0.9 1.8 2.7 3.6 4.4 5.4 6.3 7.2 7.2
CA  84391   0.0 1.2 2.3 3.5 4.6 5.7 6.9 8.1 9.3 9.3
BY  28353   0.0 0.3 0.6 0.8 1.1 1.4 1.7 1.9 2.2 2.2
TH  28332   0.0 0.4 0.8 1.3 1.7 2.1 2.5 3.0 3.4 3.4
KZ  28309   0.0 0.5 0.8 1.2 1.7 2.1 2.7 3.1 3.5 3.5
RU  28296   0.0 0.6 1.2 1.8 2.3 2.9 3.5 4.1 4.7 4.7
Exec time: 0:04:17.358113

```


#### Help with argparse:

```bash
$ python3 analize.py -h
usage: analize.py [-h]
                  app_id [results_file] [installs_file] [purchases_file]
                  [start_time] [end_time] [days]

Performs cohort analysis of installs and purchases

positional arguments:
  app_id          app_id for measurements
  results_file    output file with results, default: results.csv
  installs_file   input file with installs, default: installs.csv
  purchases_file  input file with purchases, default: purchases.csv
  start_time      start time for measurements, default: 2016-05-02 00:00:00
  end_time        end time for measurements, default: 2016-05-09 23:59:59
  days            number of days for PRI calculation, default: 10

```


## Python implementation with Pandas package

### Run example

#### Minimal running command:

```bash
$ python3 analize_pd.py 2 

```

#### Differences

This implementation has same CLI like native implementation, but under hood use Pandas python package for perform calcuations.


#### Full run example (whole output):

```bash
$ python3 analize_pd.py 2 results_pd.csv
2000000
593034
         installs
country
US         113547
UK         112569
AU          84765
NZ          84472
CA          84391
BY          28353
TH          28332
KZ          28309
RU          28296
         installs  RPI1  RPI2  RPI3  RPI4  RPI5  RPI6  RPI7  RPI8  RPI9  RPI10
country
US         113547   1.7   3.4   5.2   6.8   8.5  10.3  12.0  13.6  13.6   13.6
UK         112569   1.3   2.7   4.1   5.5   6.8   8.2   9.5  10.9  10.9   10.9
AU          84765   1.0   2.0   3.0   3.9   4.9   5.9   6.8   7.7   7.7    7.7
NZ          84472   0.9   1.8   2.7   3.6   4.4   5.4   6.3   7.2   7.2    7.2
CA          84391   1.2   2.3   3.5   4.6   5.7   6.9   8.1   9.3   9.3    9.3
BY          28353   0.3   0.6   0.8   1.1   1.4   1.7   1.9   2.2   2.2    2.2
TH          28332   0.4   0.8   1.3   1.7   2.1   2.5   3.0   3.4   3.4    3.4
KZ          28309   0.5   0.8   1.2   1.7   2.1   2.7   3.1   3.5   3.5    3.5
RU          28296   0.6   1.2   1.8   2.3   2.9   3.5   4.1   4.7   4.7    4.7
Exec time: 0:00:12.715064
```


## Go implamentation

### Run example

#### Minimal running command:

```bash
$ go build analize.go
$ ./analize installs.csv purchases.csv results_go.csv "2016-05-02 00:00:00" "2016-05-09 23:59:59" 2 10

```

#### Differences

  * Implementation in Go language
  * All CLI arguments must be provided explicitly.


#### Full run example (whole output):

```bash
$ go build analize.go
$ ./analize installs.csv purchases.csv results_go.csv "2016-05-02 00:00:00" "2016-05-09 23:59:59" 2 10
2000000
593034
Sorted by count:
US: 113547
UK: 112569
AU: 84765
NZ: 84472
CA: 84391
BY: 28353
TH: 28332
KZ: 28309
RU: 28296
1539793
457244

US  113547  1.7 3.4 5.2 6.9 8.5 10.3    12.0    13.7    13.7    13.7
UK  112569  1.4 2.8 4.1 5.5 6.8 8.2 9.5 10.9    10.9    10.9
AU  84765   1.0 2.0 3.0 4.0 5.0 5.9 6.8 7.7 7.7 7.7
NZ  84472   0.9 1.9 2.7 3.6 4.5 5.4 6.3 7.2 7.2 7.2
CA  84391   1.2 2.3 3.5 4.6 5.7 6.9 8.1 9.2 9.2 9.2
BY  28353   0.3 0.6 0.8 1.1 1.4 1.7 2.0 2.2 2.2 2.2
TH  28332   0.4 0.9 1.2 1.8 2.1 2.6 3.0 3.4 3.4 3.4
KZ  28309   0.5 0.9 1.2 1.7 2.1 2.7 3.1 3.6 3.6 3.6
RU  28296   0.6 1.2 1.8 2.4 2.9 3.5 4.2 4.7 4.7 4.7
Exec time:  7.963072265s
```



## Conclusions

### 1. Python

In native python implementation hard to use multiprocessing to optimize performance. Code readability not so good.

We can use classes and functions to improve readability and decrease performace.

### 2. Python with Pandas

Pandas inmplementation looks good and unexpectedly fast.


### 3. Go 

Go implementation looks bad, but performs fast enoght even with single thread implementation.



package main

import (
    "bufio"
    "encoding/csv"
    "os"
    "fmt"
    "strconv"
    "io"
    "flag"
    "time"
    "context"
    "log"
    "path/filepath"
    "sort"
    "strings"
)

type Installation struct {
    created int64
    mobile_app int
    country string
}

type Purchase struct {
    created int64
    mobile_app int
    country string
    install_date int64
    revenue float32
}

type Country struct {
    country string
    installs int
}

const TIME_LAYOUT = "2006-01-02 15:04:05"

var Usage = func() {
    name := filepath.Base(os.Args[0])
    fmt.Fprintf(os.Stderr, "Usage of %s:\n", name)
    fmt.Fprintf(os.Stdout, "   %s [installsFile] [purchasesFile] [resultsFile] [startTime] [endTime] [appId] [days]\n", name)
    // flag.PrintDefaults()
}

func loadSettings(ctx context.Context, logger *log.Logger) (rCtx context.Context) {

    h := flag.Bool("h", false, "Help")
    help := flag.Bool("help", false, "Help")

    flag.Parse()
    targs := flag.Args()

    if *h || *help {
        Usage()
        os.Exit(0)
    }

    if 6 > len(targs) {
        logger.Println("required arguments not provided")

        Usage()
        os.Exit(1)
    }


    installsFile := targs[0]
    purchasesFile := targs[1]
    resultsFile := targs[2]

    // check files existence
    if _, err := os.Stat(installsFile); os.IsNotExist(err) {
        file, err := os.Create(installsFile)
        if err != nil {
            // handle the error here
            log.Panicf(
                "[installsFile] file doesn't exists and can't be created: %s\n", 
                installsFile)
        }
        defer file.Close()
    }

    // check files existence
    if _, err := os.Stat(purchasesFile); os.IsNotExist(err) {
        file, err := os.Create(purchasesFile)
        if err != nil {
            // handle the error here
            log.Panicf(
                "[purchasesFile] file doesn't exists and can't be created: %s\n",
                purchasesFile)
        }
        defer file.Close()
    }

    sTime, err := time.Parse(TIME_LAYOUT, targs[3])
    if err != nil {
        log.Panicf(
            "Can't parse [startTime]: %s\n",
            targs[3])
    }

    eTime, err := time.Parse(TIME_LAYOUT, targs[4])
    if err != nil {
        log.Panicf(
            "Can't parse [endTime]: %s\n",
            targs[4])
    }

    appId, err := strconv.ParseInt(targs[5], 10, 32)
    if err != nil {
        log.Panicf("Can't parse [appId]: %s\n", targs[5])
    }

    days, err := strconv.ParseInt(targs[6], 10, 32)
    if err != nil {
        log.Panicf("Can't parse [days]: %s\n", targs[6])
    }

    rCtx = context.WithValue(ctx,  "installsFile", installsFile)
    rCtx = context.WithValue(rCtx, "purchasesFile", purchasesFile)
    rCtx = context.WithValue(rCtx, "resultsFile", resultsFile)
    rCtx = context.WithValue(rCtx, "startTime", sTime.Unix())
    rCtx = context.WithValue(rCtx, "endTime", eTime.Unix())
    rCtx = context.WithValue(rCtx, "appId", int(appId))
    rCtx = context.WithValue(rCtx, "days", int(days))

    return rCtx
}

func round(x, unit float64) float64 {
    return float64(int64(x/unit+0.5)) * unit
}

func loadInstalls(ctx context.Context, logger *log.Logger) ([]Installation, error) {

    res := []Installation{}
    inpFile := ctx.Value("installsFile").(string)
    // Load a TXT file.
    f, _ := os.Open(inpFile)

    // Create a new reader.
    r := csv.NewReader(bufio.NewReader(f))
    for i:=0; i<3000000; i++ {
        record, err := r.Read()
        // Stop at EOF.
        if err == io.EOF {
            break
        }

        if i == 0 {
            continue
        }

        t, err := time.Parse(TIME_LAYOUT, record[0])
        if err != nil {
            logger.Panic(err)
        }

        app, err := strconv.ParseInt(record[1], 10, 32)
        if err != nil {
            logger.Panic(err)
        }
        tmp := Installation{t.Unix(), int(app), strings.TrimSpace(record[2])}
        res = append(res, tmp)
    }

    return res, nil
}

func loadPurchases(ctx context.Context, logger *log.Logger) ([]Purchase, error) {

    res := []Purchase{}
    inpFile := ctx.Value("purchasesFile").(string)
    // Load a TXT file.
    f, _ := os.Open(inpFile)

    // Create a new reader.
    r := csv.NewReader(bufio.NewReader(f))
    for i:=0; i<3000000; i++ {
        record, err := r.Read()
        // Stop at EOF.
        if err == io.EOF {
            break
        }

        if i == 0 {
            continue
        }

        tmp := Purchase{}
        t, err := time.Parse(TIME_LAYOUT, record[0])
        if err != nil {
            logger.Panic(err)
        }
        tmp.created = t.Unix()

        t2, err := time.Parse(TIME_LAYOUT, record[3])
        if err != nil {
            logger.Panic(err)
        }
        tmp.install_date = t2.Unix()

        tmp.country = strings.TrimSpace(record[2])

        t3, err := strconv.ParseInt(record[1], 10, 32)
        if err != nil {
            logger.Panic(err)
        }
        tmp.mobile_app = int(t3)

        t4, err := strconv.ParseFloat(record[4], 32)
        if err != nil {
            logger.Panic(err)
        }
        tmp.revenue = float32(t4)

        res = append(res, tmp)
    }

    return res, nil
}

func checkError(message string, err error) {
    if err != nil {
        log.Fatal(message, err)
    }
}

func writeResults(ctx context.Context, logger *log.Logger, 
                  countries []Country, res map[string][]float64) error {

    file_path := ctx.Value("resultsFile").(string)
    file, err := os.Create(file_path)
    checkError("Cannot create file", err)
    defer file.Close()

    writer := csv.NewWriter(file)
    defer writer.Flush()

    head := []string{"country","installs"}
    for i:=1; i < len(res[countries[0].country]) + 1; i++ {
        head = append(head, fmt.Sprintf("PRI%d", i))
    }
    err2 := writer.Write(head)
    checkError(fmt.Sprintf("Cannot write to file: %s", file_path), err2)

    for _, c := range countries{
        value := []string{}
        value = append(value, c.country, fmt.Sprintf("%d", c.installs))
        for i:=0; i < len(res[c.country]); i++ {
            value = append(value, fmt.Sprintf("%.1f", res[c.country][i]))
        }
        err := writer.Write(value)
        checkError(fmt.Sprintf("Cannot write to file: %s", file_path), err)
    }

    return nil
}

func main() {
    exec_start := time.Now()
    var logger = log.New(os.Stdout, "", log.Ltime)

    ctx := context.Background()
    ctx = loadSettings(ctx, logger)

    df, err := loadInstalls(ctx, logger)
    if err != nil {
        logger.Panic(err)
    }

    fmt.Println(len(df))

    start := ctx.Value("startTime").(int64)
    end := ctx.Value("endTime").(int64)
    appId := ctx.Value("appId").(int)
    days := ctx.Value("days").(int)

    // select cohorta
    df2 := []Installation{}
    for _, inst := range df {
        if inst.created >= start && inst.created <= end && inst.mobile_app == appId {
            df2 = append(df2, inst)
        }
    }

    fmt.Println(len(df2))

    // group countries
    var cntr = make(map[string]*Country)
    for _, inst := range df2 {
        _, ok := cntr[inst.country]
        if ok {
            cntr[inst.country].installs++
        } else {
            t := Country{country: inst.country, installs: 1}
            cntr[inst.country] = &t
        }
    }

    // sort countries
    var countries []Country
    for _, v := range cntr {
        countries = append(countries, *v)
    }
    sort.Slice(countries, func(i, j int) bool {
        return countries[i].installs > countries[j].installs
    })
    fmt.Println("Sorted by count:")
    for _, v := range countries {
        fmt.Printf("%s: %d\n", v.country, v.installs)
    }

    pur, err := loadPurchases(ctx, logger)
    if err != nil {
        logger.Panic(err)
    }
    fmt.Println(len(pur))

    // select cohort records
    pur2 := []Purchase{}
    for _, p := range pur {
        if p.install_date >= start && p.install_date <= end && p.mobile_app == appId {
            pur2 = append(pur2, p)
        }
    }
    fmt.Println(len(pur2))

    // calc cohort
    var res = make(map[string][]float64)
    for _, c := range countries {
        for i:=1; i<days+1; i++ {
            sum := float64(0)
            cnt := 0
            end := time.Unix(int64(start), 0).Add(
                time.Duration(24.0*float64(time.Hour)*float64(i)) -
                time.Duration(time.Second))
            // fmt.Println(end)
            endt := end.Unix()
            for _, p := range pur2 {
                if int64(p.install_date) <= endt && p.country == c.country {
                    sum += float64(p.revenue)
                }
                cnt++ 
            }

            res[c.country] = append(res[c.country], round(sum / float64(c.installs), 0.05))

            // fmt.Println(c.country, end, cnt, sum)
        }
    }

    for _, c := range countries {
        fmt.Println()
        fmt.Printf("%s\t", c.country)
        fmt.Printf("%d\t", c.installs)
        for i:=0; i<days; i++ {
            fmt.Printf("%.1f\t", res[c.country][i])
        }
    }
    fmt.Println()

    err = writeResults(ctx, logger, countries, res)

    fmt.Println("Exec time: ", time.Since(exec_start))
}
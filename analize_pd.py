import pandas as pd
import time
import argparse
from datetime import timedelta


TIME_LAYOUT = '%Y-%m-%d %H:%M:%S'


def parse_args():
    parser = argparse.ArgumentParser(description='Performs cohort analysis of installs and purchases')
    parser.add_argument(
        'app_id', type=int, 
        help='app_id for measurements')

    parser.add_argument(
        'results_file', nargs='?', type=str,
        default='results.csv', 
        help='output file with results, default: results.csv')

    parser.add_argument(
        'installs_file', nargs='?', type=str,
        default='installs.csv', 
        help='input file with installs, default: installs.csv')

    parser.add_argument(
        'purchases_file', nargs='?', type=str,
        default='purchases.csv', 
        help='input file with purchases, default: purchases.csv')

    parser.add_argument(
        'start_time', nargs='?', type=str,
        default='2016-05-02 00:00:00', 
        help='start time for measurements, default: 2016-05-02 00:00:00')

    parser.add_argument(
        'end_time', nargs='?', type=str,
        default='2016-05-09 23:59:59', 
        help='end time for measurements, default: 2016-05-09 23:59:59')

    parser.add_argument(
        'days', nargs='?', type=int,
        default=10, 
        help='number of days for PRI calculation, default: 10')

    args = parser.parse_args()
    return args


def main(inst_path, purch_path, result_path, start_date, end_date, app_id, days):
    df = pd.read_csv(inst_path)
    # df.created = [pd.Timestamp(dt) for dt in df.created]
    df.created = pd.to_datetime(df.created, format=TIME_LAYOUT)
    # df.created = [pd.Timestamp(dt) for dt in df.created]
    df.mobile_app = [int(aid) for aid in df.mobile_app]

    print(len(df))

    # start = pd.Timestamp(start_date)
    start = pd.to_datetime(start_date, format=TIME_LAYOUT)
    end = pd.to_datetime(end_date, format=TIME_LAYOUT)

    # select cohort
    df = df[(df.created <= end) & (df.created >= start) & (df.mobile_app == app_id)]

    print(len(df))

    # group by country
    res = df.groupby('country').count()
    res = res.drop(columns='created')\
        .sort_values('mobile_app', ascending=False)\
        .rename(columns={"mobile_app": "installs"})

    # show installations per country
    print(res)

    # calc cohort
    pur = pd.read_csv(purch_path)
    pur.created = pd.to_datetime(pur.created, format=TIME_LAYOUT)
    pur.install_date = pd.to_datetime(pur.install_date, format=TIME_LAYOUT)

    cog = pur[(pur.install_date >= start) & \
        (pur.install_date <= end) & (pur.mobile_app == 2)]

    for i in range(1, days+1):
        end = start + pd.Timedelta(days=i, seconds=-1)
        for inx in res.index:
            res.at[inx, 'RPI{}'.format(i)] \
                = round((cog[
                        (cog.install_date <= end) & (cog.country == inx)
                    ].revenue.sum())/res.at[inx, 'installs'], 1)

    print(res)
    res.to_csv(result_path)


if __name__ == '__main__':
    args = parse_args()

    exec_st = time.monotonic()
    main(
        args.installs_file, args.purchases_file, args.results_file,
        args.start_time, 
        args.end_time,
        args.app_id,
        args.days
    )
    print('Exec time: {!s}'.format(timedelta(0, time.monotonic() - exec_st)))
